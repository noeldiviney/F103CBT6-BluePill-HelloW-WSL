target_compile_definitions(
    ${PROJECT_NAME}.elf
#    arduinolib
    PRIVATE
#    ${BUILD_VARIANT_SERIES}
#    ARDUINO=${ARDUINO_VERSION}
#    ARDUINO_ARCH_STM32
#    BOARD_NAME=${MCU_PRODUCT}
#    "VARIANT_H=\"variant_${MCU_PRODUCT}.h\" "
    ${BUILD_PRODUCT_LINE}
    -DHAL_UART_MODULE_ENABLED
)

##===========================================================================
if(FALSE) # fake a block comment
##===========================================================================

target_compile_definitions(
    arduinolib
    PUBLIC
#    -D${BUILD_VARIANT_SERIES}
    ARDUINO=${ARDUINO_VERSION}
    -DARDUINO_ARCH_STM32
    BOARD_NAME=${MCU_PRODUCT}
    VARIANT_H=\"variant_${MCU_PRODUCT}.h\"
    -D${BUILD_PRODUCT_LINE}
    -DHAL_UART_MODULE_ENABLED
)
##===========================================================================
endif() # fake a block comment
##===========================================================================

